// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import Login from "./container/LoginContainer";
import Signup from "./container/SignupContainer";
import Home from "./container/HomeContainer";
import Friend from "./container/FriendContainer";
import Invite from "./container/InviteContainer";
import Profile from "./container/ProfileContainer";
import BlankPage from "./container/BlankPageContainer";
import Sidebar from "./container/SidebarContainer";
import First from "./container/FirstContainer";
import SecureMSG from "./container/SecureMSGContainer";
import Secure from "./container/SecureContainer";
import Notification from "./container/NotificationContainer";

const Drawer = DrawerNavigator(
	{
		Home: { screen: Home },
	},
	{
		initialRouteName: "Home",
		contentComponent: props => <Sidebar {...props} />,
	}
);

const App = StackNavigator(
	{
		Login: { screen: Login },
		Signup: { screen: Signup },
		BlankPage: { screen: BlankPage},
		Drawer: { screen: Drawer},
		Friend: {screen: Friend},
		Invite: {screen: Invite},
		First: {screen: First},
		Profile: {screen: Profile},
		SecureMSG: {screen: SecureMSG},
		Secure: {screen: Secure},
		Notification: {screen: Notification},
	},
	{
		initialRouteName: "First",
		headerMode: "none",
	}
);

export default () => (
	<Root>
		<App />
	</Root>
);
