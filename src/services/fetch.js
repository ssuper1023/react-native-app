const URI = 'http://localhost:8000';

export default {
    fetchlogin(uname, pwd) {
        try {
                let response = fetch(URI + '/api/login' , {
                    method: 'POST',
                    body: JSON.stringify({
                        username: uname,
                        password: pwd,
                    }),
                });
                let responseJsonData = response.json();
                return responseJsonData;
            }
        catch(e) {
            console.log(e)
        }
    }
}