import * as React from "react";
import { Image, Platform } from "react-native";
import { Container, Content, Header, Left, Right, Body, Title, Button, Text, View, Icon, Footer,Segment } from "native-base";
//import styles from "./styles";
export interface Props {
	signupForm: any,
	onSignup: Function,
}
export interface State {
}
class Signup extends React.Component<Props, State> {
	constructor() {
		super();
		this.state = {seg : 1};
	}
	render() {
		return (
			<Container>
				<Header style={{ height: 200 }}>
					<Body style={{ alignItems: "center" }}>
						<Image source={require("../../../../assets/icon.png")} style={{ width: 104 }} />
						<Title>Help Secure your Personal &</Title>
						<Title>Business Data</Title>
					</Body>
				</Header>
				<Content>
					<View style={{backgroundColor:'#2d9cdb'}}>
						<Segment block>
							<Button active={this.state.seg === 1 ? true : false} onPress={()=>this.setState({seg : 1})}><Text>Individual</Text></Button>
							<Button active={this.state.seg === 2 ? true : false} onPress={()=>this.setState({seg : 2})}><Text>Business</Text></Button>
						</Segment>
					</View>
					{this.props.signupForm}
					<View padder>
						<Button block onPress={() => this.props.onSignup()}>
							<Text>Signup</Text>
						</Button>
					</View>
					<View padder style={{alignItems: 'center'}}>
						<View style={{flexDirection: 'row'}}>
							<Text>Do you have account </Text>
							<Text style={{color:'#2d9cdb'}} onPress={() => this.props.navigation.navigate('Login')}>Login</Text>
						</View>
					</View>
				</Content>
				<Footer style={{ backgroundColor: "#FDFDFD" }}>
					<View style={{ alignItems: "center", opacity: 0.5, flexDirection: "row" }}>
						<View padder>
							<Text style={{ color: "#000" }}>©Sealedme, 2018 </Text>
						</View>
						<Image
							source={require("../../../../assets/logo-dark.png")}
							style={{ width: 422 / 4, height: 86 / 4 }}
						/>
					</View>
				</Footer>
			</Container>
		);
	}
}

export default Signup;
