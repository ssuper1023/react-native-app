import * as React from "react";
import {
  Image,
  Dimensions
} from 'react-native';
import {
  Container,
  Header,
  Footer,
  FooterTab,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  CardItem,
  Card,
  Fab,
  View,
  Segment,
  Thumbnail,
  Input,
  Item
} from "native-base";
import styles from "./styles";
import HTML from 'react-native-render-html'
export interface Props {
  navigation: any;
  list: any;
}
export interface State {}
class Home extends React.Component<Props, State> {
  constructor() {
    super();
    this.state = {
      active: 'false',
      seg: 1
    };
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen", data={response:this.props.navigation.state.params.response})}
              />
            </Button>
          </Left>
          <Body style={{flex:3}}>
            <Title>Home</Title>
          </Body>
          <Right>
            <View style={{backgroundColor:'#2d9cdb'}}>

              <Segment style={{width: 153}}>
                <Button first active={this.state.seg === 1 ? true : false} style={{width:50}} onPress={()=>this.setState({seg : 1})}><Icon name='ios-globe-outline'/></Button>
                <Button second active={this.state.seg === 2 ? true : false} style={{width:50}} onPress={()=>this.setState({seg : 2})}><Icon name='ios-people-outline'/></Button>
                <Button last active={this.state.seg === 3 ? true : false} style={{width:50}} onPress={()=>this.setState({seg : 3})}><Icon name='ios-person-outline'/></Button>
              </Segment>
              <Body>
                <Button transparent><Icon style={{color:'#fff'}} name="ios-create-outline"/></Button>
              </Body>
            </View>
          </Right>
        </Header>
        <Content>
          <List
            dataArray={this.props.navigation.state.params.response.post}
            renderRow={data => {
              return(
                <ListItem>
                  <Card>
                    <CardItem>
                      <Left>
                      {
                        data.posts_from.image &&
                        <Thumbnail small source={{uri: "https://sealed.me/images/"+data.posts_from.image}}/>
                      }
                      </Left>
                      <Body style={{flex:3}}>
                        {
                          data.price &&
                          <Text style={{fontSize:11}}>
                            {data.posts_from.first_name} {data.posts_from.middle_name} {data.posts_from.last_name} {data.posts_from.legal_name} Send Payment To 
                            {data.posts_to.first_name} {data.posts_to.middle_name} {data.posts_to.last_name} {data.posts_to.legal_name}
                            </Text>
                        }
                        {
                          data.user_id &&
                          <Text style={{fontSize:11}}>
                            {data.posts_from.first_name} {data.posts_from.middle_name} {data.posts_from.last_name} {data.posts_from.legal_name} Post
                          </Text>
                        }
                        {
                          data.subject &&
                          <Text style={{fontSize:11}}>
                            {data.posts_from.first_name} {data.posts_from.middle_name} {data.posts_from.last_name} {data.posts_from.legal_name} Send Message To 
                            {data.posts_to.first_name} {data.posts_to.middle_name} {data.posts_to.last_name} {data.posts_to.legal_name}
                            </Text>
                        }
                        <Text note style={{fontSize:9}}>{data.created_at}</Text>
                      </Body>
                    </CardItem>
                    <CardItem cardBody>
                      <Body style={{alignItems:'center'}}>
                        {
                          data.price &&
                          <Text style={{fontSize:11}}>
                            {data.price} {data.description}
                            </Text>
                        }
                        {
                          data.user_id &&
                          <HTML html={data.text} imageMaxWidth={Dimensions.get('window').width-40}/>
                        }
                        {
                          data.subject &&
                          <Text>
                            {data.subject}
                            </Text>
                        }
                      </Body>

                    </CardItem>
                    <CardItem>
                      <Left>
                        <Button transparent>
                          <Icon name='ios-heart-outline'/>
                        </Button>
                      </Left>
                      
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Thumbnail small source={{uri: "https://sealed.me/images/"+this.props.navigation.state.params.response.user.image}}/>
                        <Body>
                          <Item rounded>
                            <Input style={{alignSelf:'stretch', height:40}} placeholder='Add Comment'/>
                          </Item>
                        </Body>
                      </Left>
                      
                    </CardItem>
                  </Card>
                </ListItem>
              );}}
            >
          </List>
        </Content>
      </Container>
    );
  }
}

export default Home;
