import * as React from "react";
import { Image, Platform, Animated } from "react-native";
import { Container, Content, Header, Body, Title, Button, Text, View, Icon, Footer } from "native-base";
//import styles from "./styles";
export interface Props {
	loginForm: any,
	onLogin: Function,
}
export interface State {
}
class Login extends React.Component<Props, State> {
	state = {
		animation: {
            formPostionLeft: new Animated.Value(795),
            loginPositionTop: new Animated.Value(1402),
            statusPositionTop: new Animated.Value(1542)
        }
	}
	componentDidMount() {
        const timing = Animated.timing;
        Animated.parallel([
            timing(this.state.animation.formPostionLeft, {
                toValue: 0,
                duration: 700
            }),
            timing(this.state.animation.loginPositionTop, {
                toValue: 0,
                duration: 700
            }),
            timing(this.state.animation.statusPositionTop, {
                toValue: 0,
                duration: 700
            })

        ]).start()
    }
	render() {
		return (
			<Container>
				<Header style={{ height: 200 }}>
					<Body style={{ alignItems: "center" }}>
						<Animated.View style={{position: 'relative', alignItems: "center", left: this.state.animation.statusPositionTop}}>
							<Image source={require("../../../../assets/icon.png")} style={{ width: 104 }} />
							<Title>Help Secure your Personal &</Title>
							<Title>Business Data</Title>
						</Animated.View>
					</Body>
				</Header>
				<Content>
					<Animated.View style={{position: 'relative', left: this.state.animation.formPostionLeft}}>
						{this.props.loginForm}
						<Button block transparent>
							<Text>Forget password?</Text>
						</Button>
					</Animated.View>
					<Animated.View style={{position: 'relative', top: this.state.animation.loginPositionTop}}>
						<View padder>
							<Button block onPress={() => this.props.onLogin()}>
								<Text>Login</Text>
							</Button>
						</View>
						<View padder style={{alignItems: 'center'}}>
							<View style={{flexDirection: 'row'}}>
								<Text>Do not have account </Text>
								<Text style={{color:'#2d9cdb'}} onPress={() => this.props.navigation.navigate('Signup')}>Signup</Text>
							</View>
						</View>
					</Animated.View>
				</Content>
				<Footer style={{ backgroundColor: "#FDFDFD" }}>
					<Animated.View style={{position: 'relative', top: this.state.animation.loginPositionTop}}>	
						<View style={{ alignItems: "center", opacity: 0.5, flexDirection: "row" }}>
							<View padder>
								<Text style={{ color: "#000" }}>©Sealedme, 2018 </Text>
							</View>
							<Image
								source={require("../../../../assets/logo-dark.png")}
								style={{ width: 422 / 4, height: 86 / 4 }}
							/>
						</View>
					</Animated.View>
				</Footer>
			</Container>
		);
	}
}

export default Login;
