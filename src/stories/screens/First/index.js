import * as React from "react";
import {Image, Animated} from 'react-native';
import {
 Text,
 Container,
 Content, 
 Icon, 
 Thumbnail, 
 Left, 
 Body, 
 Right, 
 Header, 
 Button, 
 Title, 
 View, 
} from "native-base";
import { NavigationActions } from "react-navigation";

export interface Props {
	navigation: any,
}
export interface State {}
export default class First extends React.Component<Props, State> {
    state = {
        animation: {
            formPostionLeft: new Animated.Value(795),
            logoPositionTop: new Animated.Value(400),
            statusPositionTop: new Animated.Value(30)
        }
    }
    componentDidMount() {
        const timing = Animated.timing;
        Animated.parallel([
            timing(this.state.animation.formPostionLeft, {
                toValue: 0,
                duration: 700
            }),
            timing(this.state.animation.logoPositionTop, {
                toValue: 0,
                duration: 700
            }),
            timing(this.state.animation.statusPositionTop, {
                toValue: 0,
                duration: 2000
            })
        ]).start()
    }
	render() {
		return (
			<Container>
                <View style={{position: 'absolute', top: 0, left: 0, width: '100%', height: '100%'}}>
                    <Image style={{ flex: 1}}
                    source={require('../../../../assets/app-background.jpg')}
                    />
                </View>
                <View style={{ marginTop: 50, alignItems: "center", opacity:0.8 }}>
                    <Animated.View style={{position: 'relative', alignItems: "center", left: this.state.animation.logoPositionTop}}>
                        <Image source={require("../../../../assets/icon.png")} style={{ width: 200, height: 200 }} />
                        <Title>Help Secure your Personal &</Title>
                        <Title>Business Data</Title>
                    </Animated.View>
                </View>
                <View style={{ marginTop: 80, alignItems: "center" }}>
                    <Animated.View style={{position: 'relative', alignItems: "center", bottom: this.state.animation.statusPositionTop}}>
                        <Button transparent>
                            <Icon name = 'ios-arrow-down' style={{fontSize: 60, color:'#fff'}}  onPress={() => this.props.navigation.navigate("Login")}/>
                        </Button>
                    </Animated.View>
                </View>
                
    		</Container>
    	);
	}
}
