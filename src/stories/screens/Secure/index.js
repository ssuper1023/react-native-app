import * as React from "react";
import { Image } from "react-native";
import { Text, Container, List, ListItem, Content, Icon, Thumbnail, Left, Body, Right, Header, Button, Title, Item, Input} from "native-base";
import { NavigationActions } from "react-navigation";

const routes = [
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "SecureMSG",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
];

export interface Props {
	navigation: any,
}
export interface State {}
export default class Secure extends React.Component<Props, State> {
	render() {
		return (
			<Container>
                <Header searchBar rounded>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="ios-arrow-back" />
                        </Button>
                    </Left>

                    <Body style={{ flex: 3 }}>
                        <Title>SecureMSG</Title>
                    </Body>

                    <Right style= {{flex:6}}>
                        <Item rounded>
                            <Icon style={{color:'#fff'}} name='ios-search'/>
                            <Input style={{color:'#fff'}} palceholder="Search"/>
                            <Icon style={{color:'#fff'}} name='ios-people'/>
                        </Item>
                    </Right>
                </Header>
                <Content>
                    <Button transparent onPress={() => this.props.navigation.navigate("SecureMSG", {response: this.props.navigation.state.params.response})}>
                        <Text>Compose</Text>
                    </Button>
                    <List
                        style={{ marginTop: 10 }}
                        dataArray={this.props.navigation.state.params.msglist}
                        renderRow={data => {
                            return (
                                <ListItem
                                    avatar
                                    style={{margin:10}}
                                    
                                    >
                                    <Left>
                                        <Text>{data.subject}</Text>
                                    </Left>
                                    <Body>
                                        <Text>{data.first_name} {data.last_name}{data.legal_name}</Text>
                                        <Text note>member of sealed</Text>
                                    </Body>
                                    <Right>
                                        <Image source={require('../../../../assets/env.png')} />
                                    </Right>
                                </ListItem>
                            );
                        }}
                    />
                </Content>
            </Container>
		);
	}
}
