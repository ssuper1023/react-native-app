import * as React from "react";
import {Image, View} from 'react-native';
import {
 Text,
 Container,
 List,
 ListItem, 
 Content, 
 Icon, 
 Thumbnail, 
 Left, 
 Body, 
 Right, 
 Header, 
 Button, 
 Title, 
 Grid, 
 Row, 
 Card, 
 CardItem,
 Tabs,
 Tab,
 TabHeading
} from "native-base";
import { NavigationActions } from "react-navigation";

const routes = [
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "Friend",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "Friend",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/d1.jpg"),
        name: "Emilly Barbosa",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        route: "Friend",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        route: "BlankPage",
        photo: require("../../../../assets/c2.jpg"),
        name: "David Massie"
    },
];

export interface Props {
	navigation: any,
}
export interface State {}
export default class Profile extends React.Component<Props, State> {
	render() {
		return (
			<Container>
                <Header style={{height:300}}>
                    <Grid>
                        <Row style ={{height:50}}>
                            <Left>
                                <Button transparent onPress={() => this.props.navigation.goBack()}>
                                   <Icon name="ios-arrow-back" />
                                </Button>
                            </Left>
                            <Body style={{ flex: 3 }}>
                                <Title>Profile</Title>
                            </Body>
                            <Right />
                        </Row>
                        <Row style={{height: 200, backgroundColor:'#2d9cdb'}}>
                            <Body style={{alignItems:'center'}}>
                                <Image source={{uri: "https://sealed.me/images/"+this.props.navigation.state.params.response.user.image}} style={{width:144, height:144, borderRadius:72}} />
                                <Title>{this.props.navigation.state.params.response.user.first_name} {this.props.navigation.state.params.response.user.last_name}{this.props.navigation.state.params.response.user.legal_name}</Title>
                                <Text note style={{color:'#fff'}}>{this.props.navigation.state.params.response.user.city}, {this.props.navigation.state.params.response.user.country}</Text>    
                            </Body>
                        </Row>
                            
                        <Row style={{backgroundColor:'#2d9cdb', alignItems:'center'}}>
                                <Title>{this.props.navigation.state.params.response.user.followers.length}</Title>
                                <Title style={{fontSize: 12}}>Friend</Title>
                        </Row>
                    </Grid>
                </Header>
				<Content>
                    
                    <Tabs>
                        <Tab heading={<TabHeading><Text>Activity</Text></TabHeading>}>
                            <List
                                dataArray={routes}
                                renderRow={data => {
                                return (
                                    <ListItem
                                        style={{margin:10, marginRight:0}}
                                        onPress={() => {
                                            this.props.navigation.navigate(data.route);
                                        }}
                                    >
                                        <Card>
                                            <CardItem cardBody style={{ alignItems: "center"}}>
                                                <Image source={data.photo} style={{height:200, width: null, flex: 1}}/>
                                            </CardItem>
                                            <CardItem style={{ alignItems: "center"}}>
                                                <Text>{data.name}</Text>
                                            </CardItem>
                                            <CardItem>
                                                <Left>
                                                  <Button transparent>
                                                    <Icon active name="thumbs-up" />
                                                    <Text>12 Likes</Text>
                                                    <Icon active name="chatbubbles" />
                                                    <Text>4 Comments</Text>
                                                  </Button>
                                                </Left>
                                                <Right>
                                                  <Text>11h ago</Text>
                                                </Right>
                                            </CardItem>
                                        </Card>
                                    </ListItem>
                                    );
                                }}
                            />
                        </Tab>
                        <Tab heading={<TabHeading><Icon name="ios-person"/><Text>Info</Text></TabHeading>}>
                            <List>
                                <ListItem icon>
                                    <Left><Icon name='ios-calendar-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.bd}</Text></Body>
                                </ListItem>
                                <ListItem icon>
                                    <Left><Icon name='ios-transgender-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.gender}</Text></Body>
                                </ListItem>
                                <ListItem icon>
                                    <Left><Icon name='ios-pin-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.street_address},{this.props.navigation.state.params.response.user.city} {this.props.navigation.state.params.response.user.state}</Text></Body>
                                </ListItem>
                                <ListItem icon>
                                    <Left><Icon name='ios-call-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.phone_num}</Text></Body>
                                </ListItem>
                                <ListItem icon>
                                    <Left><Icon name='ios-information-circle-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.occupation}</Text></Body>
                                </ListItem>
                                <ListItem icon>
                                    <Left><Icon name='ios-man-outline'/></Left>
                                    <Body><Text>{this.props.navigation.state.params.response.user.employer}</Text></Body>
                                </ListItem>
                            </List>
                        </Tab>
                    </Tabs>
            	</Content>
    		</Container>
    	);
	}
}
