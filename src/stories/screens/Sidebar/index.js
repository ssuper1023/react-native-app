import * as React from "react";
import { Image} from 'react-native';
import { Text, Container, List, ListItem, Content, Icon, Header, Left, Body, Right, Title, Badge } from "native-base";
import { NavigationActions } from "react-navigation";

const routes = [
    {
        route: "Home",
        caption: " Home",
        name: "ios-home-outline",
    },
    {
        route: "Profile",
        caption: " Profile",
        name: "ios-person-outline",
    },
    {
        route: "Friend",
        caption: " Friend",
        name: "ios-people-outline",
    },
    {
        route: "Invite",
        caption: " Invite",
        name: "ios-person-add-outline"
    },
    {
        route: "Secure",
        caption: " SecureMSG",
        name: "ios-mail-outline"
    },
    {
        route: "Login",
        caption: " Logout",
        name: "ios-log-out"
    },
];


export interface Props {
	navigation: any,
}
export interface State {}
const resetAction = NavigationActions.reset({
	index: 0,
	actions: [NavigationActions.navigate({ routeName: "Login" })],
});
export default class Sidebar extends React.Component<Props, State> {
	render() {
		return (
			<Container>
                <Header>
                    <Left>
                        {
                            this.props.navigation.state.params.response.user.image &&
                            <Image source={{uri: "https://sealed.me/images/"+this.props.navigation.state.params.response.user.image}} style={{width:50, height:50, borderRadius:25}} />
                        }
                        {
                            !this.props.navigation.state.params.response.user.image &&
                            <Image source={{uri: "https://sealed.me/images/deault-logo.png"}} style={{width:50, height:50, borderRadius:25}} />   
                        }
                    </Left>
                    <Body>
                        <Title style={{fontSize: 15}}>
                            {this.props.navigation.state.params.response.user.first_name} {this.props.navigation.state.params.response.user.last_name}
                        </Title>
                    </Body>
                    <Right>
                        <Icon name = 'ios-settings-outline' style={{color:'#fff'}} />
                    </Right>
                </Header>
				<Content>
					<List
						style={{ marginTop: 15 }}
						dataArray={routes}
						renderRow={data => {
							return (
								<ListItem
									button
									onPress={() => {
										data.route === "Login"
											? this.props.navigation.dispatch(resetAction)
											: this.props.navigation.navigate(data.route, {response: this.props.navigation.state.params.response});
									}}
								>
                                    <Left>
    									<Icon name = {data.name}></Icon>
    									<Text>{data.caption}</Text>
                                    </Left>
								</ListItem>
							);
						}}
					/>
				</Content>
			</Container>
		);
	}
}
