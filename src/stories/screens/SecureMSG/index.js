import * as React from "react";
import { StyleSheet } from 'react-native';
import { Text, Container, List, ListItem, Content, Icon, Thumbnail, Footer, FooterTab,
    Left, Body, Right, Header, Button, Title, Input, Badge, View, Segment, Toast } from "native-base";
import { NavigationActions } from "react-navigation";
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import Timeline from 'react-native-timeline-listview'

const s = StyleSheet.create({
    label: {
        color: "black",
        fontSize: 12,
    },
    input: {
        fontSize: 16,
        color: "black",
    },
});
_onChange = formData => {
    /* eslint no-console: 0 */
    console.log(JSON.stringify(formData, null, " "));
};

_onFocus = field => {
    /* eslint no-console: 0 */
    console.log(field);
};
export interface Props {
	navigation: any,
}
export interface State {}
export default class SecureMSG extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        let param = props.navigation.state.params.response;
        this.state = {
            addpay : 0,
            addcal : 0,
            to: "",
            subject: "",
            first_name: param.user.first_name,
            last_name: param.user.last_name,
            legal_name: param.user.legal_name,
            city: param.user.city,
            state: param.user.state,
            country: param.user.country,
            address: param.user.street_address,
            zip_code: param.user.zip_code,
            medical_id: "",
            social_id: "",
            phone: param.user.phone_num,
            web: "",
            email: param.user.email
        }
        this.data = [
          {time: '09:00', title: 'Archery Training', description: 'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. ', circleColor: '#009688',lineColor:'#009688'},
          {time: '10:45', title: 'Play Badminton', description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.'},
          {time: '12:00', title: 'Lunch'},
          {time: '14:00', title: 'Watch Soccer', description: 'Team sport played between two teams of eleven players with a spherical ball. ',lineColor:'#009688'},
          {time: '16:30', title: 'Go to Fitness center', description: 'Look out for the Best Gym & Fitness Centers around me :)', circleColor: '#009688'}
        ]
    }
    _onChange = formData => {
        /* eslint no-console: 0 */
        console.log(JSON.stringify(formData, null, " "));
    };
    send(){
        fetch('https://sealed.me/api/msg/send' , {
            method: 'POST',
             headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization' : 'Bearer ' + this.props.navigation.state.params.response.token
            },
            body: JSON.stringify({
                to: this.state.to,
                subject: this.state.subject,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                legal_name: this.state.legal_name,
                city: this.state.city,
                state: this.state.state,
                country: this.state.country,
                street_address: this.state.address,
                zip_code: this.state.zip_code,
                medical_id: this.state.medical_id,
                social_security: this.state.social_id,
                phone: this.state.phone,
                web: this.state.web,
                email: this.state.email,
                status: 'show'
            }),
        }).then((response) => response.json())
        .then((response) => {
            if(response.success)
                Toast.show({
                text: "Success",
                duration: 2000,
                position: "top",
                textStyle: { textAlign: "center" }
                });
            if(response.error)
                Toast.show({
                text: "Error",
                duration: 2000,
                position: "top",
                textStyle: { textAlign: "center" }
                });
        });
    }
	render() {
        let param = this.props.navigation.state.params.response;
		return (
			<Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="ios-arrow-back" />
                        </Button>
                    </Left>

                    <Body style={{ flex: 3 }}>
                        <Title>Secure Message</Title>
                    </Body>
                    <Right />
                </Header>
    			<Content>
                    
                        <List style={{flex:1}}>
                            <ListItem><Input placeholder = "To" onChangeText={(text)=>this.setState({to: text})}/></ListItem>
                            <ListItem><Input placeholder = "Subject" onChangeText={(text)=>this.setState({subject: text})}/></ListItem>
                            <ListItem><Input placeholder = "First Name" value = {param.user.first_name} onChangeText={(text)=>this.setState({first_name: text})}/></ListItem>
                            <ListItem><Input placeholder = "Last Name" value = {param.user.last_name} onChangeText={(text)=>this.setState({last_name: text})}/></ListItem>
                            <ListItem><Input placeholder = "Legal Name" value = {param.user.legal_name} onChangeText={(text)=>this.setState({legal_name: text})}/></ListItem>
                            <ListItem><Input placeholder = "City" value = {param.user.city} onChangeText={(text)=>this.setState({city: text})}/></ListItem>
                            <ListItem><Input placeholder = "State" value = {param.user.state} onChangeText={(text)=>this.setState({state: text})}/></ListItem>
                            <ListItem><Input placeholder = "Country" value = {param.user.country} onChangeText={(text)=>this.setState({country: text})}/></ListItem>
                            <ListItem><Input placeholder = "Address" value = {param.user.street_address} onChangeText={(text)=>this.setState({address: text})}/></ListItem>
                            <ListItem><Input placeholder = "Zip Code" value = {param.user.zip_code} onChangeText={(text)=>this.setState({zip_code: text})}/></ListItem>
                            <ListItem><Input placeholder = "Email" value = {param.user.email} onChangeText={(text)=>this.setState({email: text})}/></ListItem>
                            <ListItem><Input placeholder = "Social ID" onChangeText={(text)=>this.setState({social_id: text})}/></ListItem>
                            <ListItem><Input placeholder = "Medical ID" onChangeText={(text)=>this.setState({medical_id: text})}/></ListItem>
                            <ListItem><Input placeholder = "Web" onChangeText={(text)=>this.setState({web: text})}/></ListItem>
                            <ListItem><Input placeholder = "Phone" value = {param.user.phone_num} onChangeText={(text)=>this.setState({phone: text})}/></ListItem>
                        </List>
                    
                    {
                        this.state.addpay === 1 &&
                        <CreditCardInput
                            autoFocus

                            requiresName
                            requiresCVC
                            requiresPostalCode

                            labelStyle={s.label}
                            inputStyle={s.input}
                            validColor={"black"}
                            invalidColor={"red"}
                            placeholderColor={"darkgray"}

                            onFocus={this._onFocus}
                            onChange={this._onChange}
                        />

                    }
                    {
                        this.state.addcal === 1 &&
                        <Timeline
                            data={this.data}
                            circleSize={20}
                            circleColor='rgb(45,156,219)'
                            lineColor='rgb(45,156,219)'
                            timeContainerStyle={{minWidth:52, marginTop: -5}}
                            timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
                            descriptionStyle={{color:'gray'}}
                            options={{
                                style:{paddingTop:10}
                            }}
                        />
                    }
                    
    			</Content>
                <Footer>
                    <FooterTab>
                        <Button active = {this.state.addpay === 1 ? true : false} onPress = {()=>this.setState({addpay:1 - this.state.addpay})}>
                            <Icon name='ios-card'/>
                            <Text>Add Payment</Text>
                        </Button>
                        <Button active = {this.state.addcal === 1 ? true : false} onPress = {()=>this.setState({addcal:1 - this.state.addcal})}>
                            <Icon name='ios-calendar'/>
                            <Text>Schedule Event</Text>
                        </Button>
                        <Button onPress={()=>this.send()}>
                            <Icon name='ios-arrow-round-forward'/>
                            <Text>Send Now</Text>
                        </Button>
                    </FooterTab>
                </Footer>
    		</Container>
		);
	}
}
