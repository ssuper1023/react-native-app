import * as React from "react";
import { Text, Container, List, ListItem, Content, Icon, Thumbnail, Left, Body, Right, Header, Button, Title, Item, Input } from "native-base";
import { NavigationActions } from "react-navigation";


export interface Props {
	navigation: any,
}
export interface State {}
export default class Invite extends React.Component<Props, State> {
    inviteFriend(id) {
        fetch('https://sealed.me/api/invite/send' , {
            method: 'POST',
             headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization' : 'Bearer ' + this.props.navigation.state.params.response.token
            },
            body: JSON.stringify({
                id:id
            }),
        }).then((response) => response.json())
        .then((response) => {
            if(response.success)
                Toast.show({
                text: "Success",
                duration: 2000,
                position: "top",
                textStyle: { textAlign: "center" }
                });
            if(response.error)
                Toast.show({
                text: "Error",
                duration: 2000,
                position: "top",
                textStyle: { textAlign: "center" }
                });
        });
    }
	render() {
		return (
			<Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="ios-arrow-back" />
                        </Button>
                    </Left>

                    <Body style={{ flex: 3 }}>
                        <Title>Invite</Title>
                    </Body>

                    <Right style= {{flex:6}}>
                        <Item rounded>
                            <Icon style={{color:'#fff'}} name='ios-search'/>
                            <Input style={{color:'#fff'}} palceholder="Search"/>
                            <Icon style={{color:'#fff'}} name='ios-people'/>
                        </Item>
                    </Right>
                </Header>
				<Content>
					<List
						style={{ marginTop: 10 }}
						dataArray={this.props.navigation.state.params.response.may_know}
						renderRow={data => {
							return (
								<ListItem
                                  avatar
                                  style={{margin:10}}
								>
                                     <Body>
				   					    <Text>{data.first_name}{data.last_name}{data.legal_name}</Text>
                                        <Text note>member of sealed</Text>
                                    </Body>
                                    <Right>
                                        <Text style={{color:'#2d9cdb'}}
                                        onPress={()=>this.inviteFriend(data.id)}>
                                         +Add Friend </Text>
                                    </Right>
								</ListItem>
							);
						}}
					/>
				</Content>
			</Container>
		);
	}
}
