import * as React from "react";
import { Text, Container, List, ListItem, Content, Icon, Thumbnail, Left, Body, Right, Header, Button, Title,View } from "native-base";
import { NavigationActions } from "react-navigation";

const notification_data = [
    {
        notification_type: "friend_request",
        route: "BlankPage",
        photo: require("../../../../assets/image_13.jpg"),
        name: "Sam Bellows",
    },
    {
        notification_type: "new_post",
        route: "BlankPage",
        photo: require("../../../../assets/d1.jpg"),
        post: require("../../../../assets/images.png"),
        name: "Emilly Barbosa",
    },
    {
        notification_type: "viewed_profile",
        route: "Notification",
        photo: require("../../../../assets/image_14.jpg"),
        name: "Sinikka Oramo",
    },
    {
        notification_type: "accepted_invite",
        route: "Notification",
        photo: require("../../../../assets/c1.jpg"),
        name: "Samsa Parras",
    },
    {
        notification_type: "message_send",
        route: "BlankPage",
        name: "Successfully Send your Secure Message"
    },
];

export interface Props {
	navigation: any,
}
export interface State {}
export default class Notification extends React.Component<Props, State> {
	render() {
		return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="ios-arrow-back" />
                        </Button>
                    </Left>

                    <Body style={{ flex: 3 }}>
                        <Title>Notification</Title>
                    </Body>

                    <Right />
                </Header>
                <Content>
                    <List
                        style={{ marginTop: 10 }}
                        dataArray={notification_data}
                        renderRow={data => {
                            return (
                                <ListItem
                                    avatar
                                    style={{margin:10}}
                                    onPress={() => {
                                        this.props.navigation.navigate(data.route);
                                    }}
                                >
                                
                                <Left>
                                    {
                                        data.notification_type!='message_send' &&
                                        <Thumbnail source={data.photo}/>
                                    }

                                    <Text>{data.name}</Text>
                                    <Text note>{data.notification_type}</Text>
                                </Left>
                                </ListItem>
                            );
                        }}
                    />
                </Content>
            </Container>
		);
	}
}
