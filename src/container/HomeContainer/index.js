// @flow
import * as React from "react";
import { connect } from "react-redux";
import {View} from "react-native";
import {Toast} from "native-base";
import Home from "../../stories/screens/Home";
import datas from "./data";
import { fetchList } from "./actions";
export interface Props {
	navigation: any,
	fetchList: Function,
}
export interface State {}
class HomeContainer extends React.Component<Props, State> {
	values: any;

	constructor(props) {
	    super(props);
	}
	
	render() {
		
		return <Home navigation={this.props.navigation}/>;
	}
}



function bindAction(dispatch) {
	return {
		fetchList: url => dispatch(fetchList(url)),
	};
}

const mapStateToProps = state => ({
	isLoading: state.homeReducer.isLoading,
});

export default connect(mapStateToProps, bindAction)(HomeContainer);

