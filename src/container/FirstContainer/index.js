// @flow
import * as React from "react";
import First from "../../stories/screens/First";
export interface Props {
	navigation: any,
}
export interface State {}
export default class FirstContainer extends React.Component<Props, State> {
	render() {
		return <First navigation={this.props.navigation} />;
	}
}
