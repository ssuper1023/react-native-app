// @flow
import * as React from "react";
import SecureMSG from "../../stories/screens/SecureMSG";
export interface Props {
	navigation: any,
}
export interface State {}
export default class SecureMSGContainer extends React.Component<Props, State> {
	render() {
		return <SecureMSG navigation={this.props.navigation} />;
	}
}
