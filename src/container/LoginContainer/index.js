// @flow
import * as React from "react";
import { Item, Input, Icon, Toast, Form } from "native-base";
import { Field, reduxForm } from "redux-form";
import Login from "../../stories/screens/Login";
import fetchlogin from '../../services/fetch';

const required = value => (value ? undefined : "Required");
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;
const maxLength25 = maxLength(25);
const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength6 = minLength(6);
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? "Only alphanumeric characters"
    : undefined;

export interface Props {
  navigation: any;
}
export interface State {}
class LoginForm extends React.Component<Props, State> {
  textInput: any;
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    }

    
  }
  setEmailStatus(event) {
    this.setState({email: event.target.value});
  }
  setPasswordStatus(event) {
    this.setState({password: event.target.value});
  }
  
  login() {
    let self = this;
    if (this.props.valid) {
          
        fetch('https://sealed.me/api/login' , {
            method: 'POST',
             headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            }),
        }).then((response) => response.json())
        .then((response) => {
            let token = response.token;
            if (response.token) {
              fetch('https://sealed.me/api/infos' , {
                  method: 'GET',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + response.token
                  }
              }).then((response) => response.json())
              .then((response) => {
                  if (response) {
                    let values = response;
                    values.token = token;
                    //this.props.data = response;
                    let c = 0, c1 = 0, c2 = 0;
                    for(key in values.post){
                      c ++;
                    }
                    for (i = 0; i < c; i ++){
                      if(values.post[i].user_id){
                        
                        for(j = 0; j < values.all_user.length; j ++)
                        {
                          if(values.all_user[j].id == values.post[i].user_id)
                          {
                            values.post[i].posts_from = values.all_user[j];
                            break;
                          }
                        }
                        c1++;
                      }
                      else{
                        for(j = 0; j < values.all_user.length; j ++)
                        {
                          if(values.all_user[j].id == values.post[i].from)
                          {
                            values.post[i].posts_from = values.all_user[j];
                          }
                          if(values.all_user[j].id == values.post[i].to)
                          {
                            values.post[i].posts_to = values.all_user[j]; 
                          }
                        }
                       c2++;
                      }
                    }

                    
                    this.props.navigation.navigate("Drawer", data={response:values});
                  }
                  else {

                    Toast.show({
                    text: token,
                    duration: 2000,
                    position: "top",
                    textStyle: { textAlign: "center" }
                    });
                  }
                }
              );
            }
            else {
              Toast.show({
              text: "Email or Password not match!",
              duration: 2000,
              position: "top",
              textStyle: { textAlign: "center" }
              });
            }
          });
            
    } else {
      Toast.show({
        text: "Enter Valid Email & password!",
        duration: 2000,
        position: "top",
        textStyle: { textAlign: "center" }
      });
    }
  }
  getInfo(id, infos){
    
  }
  render() {


    const form = (
      <Form>
        <Item>
          <Icon active name="person" />
          <Input
            value = {this.state.email}
            placeholder="Email"
            onChangeText={(text)=>this.setState({email: text})}
          />
        </Item>
        <Item>
          <Icon active name="unlock" />
          <Input
            value = {this.state.password}
            placeholder="Password"
            secureTextEntry
            onChangeText={(text)=>this.setState({password: text})}
          />
        </Item>
      </Form>
    );
    return (
      <Login
        navigation={this.props.navigation}
        loginForm={form}
        onLogin={() => this.login()}
      />
    );
  }
}
const LoginContainer = reduxForm({
  form: "login"
})(LoginForm);
export default LoginContainer;
