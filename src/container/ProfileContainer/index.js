// @flow
import * as React from "react";
import Profile from "../../stories/screens/Profile";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ProfileContainer extends React.Component<Props, State> {
	render() {
		return <Profile navigation={this.props.navigation} />;
	}
}
