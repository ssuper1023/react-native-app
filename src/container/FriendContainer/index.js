// @flow
import * as React from "react";
import Friend from "../../stories/screens/Friend";
export interface Props {
	navigation: any,
}
export interface State {}
export default class FriendContainer extends React.Component<Props, State> {
	render() {
		return <Friend navigation={this.props.navigation} />;
	}
}
