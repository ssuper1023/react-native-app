// @flow
import * as React from "react";
import Invite from "../../stories/screens/Invite";
export interface Props {
	navigation: any,
}
export interface State {}
export default class InviteContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props)
	}
	render() {
		var all_user = this.props.navigation.state.params.response.all_user;
		var user_id = this.props.navigation.state.params.response.user.id;
		var followers = this.props.navigation.state.params.response.user.followers;
		var cnt_user = 0, cnt_flw = 0;
		for (key in all_user) {
			cnt_user ++;
		}
		for (key in followers) 
			cnt_flw ++;
		var i= 0, j = 0, k = 0;
		var may_know = {}

		for (i = 0; i < cnt_user; i ++) {
			for (j = 0; j < cnt_flw; j++) {
				if (all_user[i].id == followers[j].id || all_user[i].id == user_id) {
					break;
				}
			}
			if (j == cnt_flw) {
				may_know[k ++] = all_user[i];
			}
		}
		this.props.navigation.state.params.response.may_know = may_know;
		return <Invite navigation={this.props.navigation} />;
	}
}
