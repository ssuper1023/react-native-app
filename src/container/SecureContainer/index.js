// @flow
import * as React from "react";
import {Toast} from "native-base";
import Secure from "../../stories/screens/Secure";
export interface Props {
	navigation: any,
}
export interface State {}
export default class SecureContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props);
	}
	render() {
		var msglist = [];
		var c = 0, l = 0;
		var post = this.props.navigation.state.params.response.post;
		var user_id = this.props.navigation.state.params.response.user.id;
		for(key in post){
          l ++;
        }
		for (var i = 0; i < l; i++) {
			if(post[i].subject)
				if(post[i].to == user_id)
					msglist[c++] = post[i];
		}
		this.props.navigation.state.params.msglist = msglist;
		return <Secure navigation={this.props.navigation}/>;
	}
}
